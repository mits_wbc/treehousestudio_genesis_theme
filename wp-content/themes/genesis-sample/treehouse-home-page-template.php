<?php
/**
* Template Name: Home Page Template
* Description: Template used for the home page
*/

//* Add custom body class
add_filter( 'body_class', 'treehousestudio_home_body_class' );
function treehousestudio_home_body_class( $classes ) {
	$classes[] = 'home-page';
	return $classes;
}

//* Remove Footer Widgets
remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );

genesis();