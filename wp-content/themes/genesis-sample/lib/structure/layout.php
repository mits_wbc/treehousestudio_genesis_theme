<?php

// Body Content
add_action ( 'genesis_my_content', 'genesis_mycode_content', 5 );
function genesis_mycode_content() {
	echo '<section id="hints">
			<a href="#" class="freeqoute">Get a free quote</a>
			<div class="container">
				<div class="row">
				<div class="col-lg-7 text-right">
					<h3>We always offer free phone consultation</h3>
				</div>
				<div class="col-lg-5 text-left"><h3><span><strong>+34 453 345 543</strong></span></h3></div>
				<div class="col-lg-7 text-right">
					<h3>For quick estimate fill out our short online</h3>
				</div>
				<div class="col-lg-5 text-left">
					<h3><span><strong><a href="#">INQUIRY FORM</a></strong></span></h3>
				</div>
					
				</div>
			</div>
		</section>

		<section id="services">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<h2>We offer one stop shop for website design and development services</h2>
					</div>
					<div class="col-lg-6 col-md-6"></div>
					<div class="col-lg-6 col-md-6"></div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<div class="thumbnail">
							<img src="img/maintance.png" alt="Web maintance">
							<div class="caption">
								<h3>WEBSITE MAINTANCE</h3>
								<p>While our sites are built with a CMS for easy self-ediing, most cllients have the need for advanced updates and technical support througout the year. Additionaly graphic design for online and offline media is also needed to further enchance your brand image and attract customers.</p>      
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-6">
						<div class="thumbnail">
							<img src="img/dev.png" alt="Web maintance">
							<div class="caption">
								<h3>WEBSITE DEVELOPMENT</h3>
								<p>Every business needs a visually appealing and functional website design to compete in the ever changing online environment. To succeed, you need something trully special that differentiates you from your competitors and makes you stand out from the crowd!<br><br>

									We know your business is unique, which is why we partner with you to create a custom website that reflects your business and fits your needs, not someone else’s.  </p>      
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="estimation">
			<div class="white-mask">
				<div class="container text-center">
					<div class="row">
						<div class="col-lg-6">
							<span><img src="img/shape1.png" width="48px"></span>
							<h2>Quick website estimate form</h2>
							<p>Fill out this form and get a quick estimate on your<br>project within 24 hours.</p>
							<div class="shape"><img src="img/estimate.png" width="80%"></div>
						</div>
						<div class="col-lg-6">
							<form>
								<input type="text" name="name" placeholder="Your name"><br>
								<textarea placeholder="Your website info"></textarea><br>
								<h3><strong>I need quick estimate on (check all that apply):</strong></h3><br>
								<input type="checkbox" id="checkone" name="new" value="new website"> <label for="checkone">Building a brand new website</label><br>
								<input type="checkbox" id="checktwo" name="redesign" value="redesign website"> <label for="checktwo">Redesign an existing website</label><br>
								<input type="checkbox" id="checkthree" name="maintance" value="maintance"> <label for="checkthree">Regular website maintance</label><br>
								<input type="checkbox" id="checkfour" name="consultation" value="consultation"> <label for="checkfour">Online strategy (free) consultation</label><br>
								<p><a href="#" class="btn btn-xl btn-orange" role="button">Next</a></p>
							</form>		
						</div>
					</div>
				</div>
				</div>
			</section>';
}