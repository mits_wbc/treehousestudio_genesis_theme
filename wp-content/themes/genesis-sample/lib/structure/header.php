<?php
add_action ( 'genesis_my_header', 'genesis_mycode_header', 5 );
function genesis_mycode_header()
{
echo '<div class="main" id="hero">
			<div class="mask-layer">
				<div class="col-lg-12 col-md-12">
					<div class="navbar navbar-default navbar-transparent" role="navigation" data-start="padding: 10px 0px; background: rgba(255, 255, 255, .1); border-color: rgba(255, 255, 255, .1);" data-20p="padding: 0px 0px; background: rgba(255, 255, 255, .95); border-color: rgba(231, 231, 231, 1);">
						<div class="container">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<!-- Light and dark logo -->
								
							</div>
							<!-- Navigation links -->
							<div class="collapse navbar-collapse" id="navbar-collapse">
								<ul class="nav navbar-nav navbar-right">
									<li class="active"><a href="index.html"  data-start="color: rgb(255, 255, 255);" data-20p="color: rgb(0, 0, 0);">ABOUT</a></li>
									<li><a href="about.html" data-start="color: rgb(255, 255, 255);" data-20p="color: rgb(255, 255, 255);">SERVICES</a></li>
									<li><a href="history.html" data-start="color: rgb(255, 255, 255);" data-20p="color: rgb(255, 255, 255);">GET A QUOTE</a></li>
									<li><a href="people.html" data-start="color: rgb(255, 255, 255);" data-20p="color: rgb(255, 255, 255);">CONTACT</a></li>														
								</ul>
							</div>
						</div>
					</div></div>
					<div>
						<div class="middle_box">
							<img src="'.get_stylesheet_directory_uri().'/images/logo.png" width="232px" alt="">
							<h4>Your home for digital growth</h4>
							<p class="btn-large btn-lined-white"><span></span><a href="#">CONTACT US</a></p>
						</div>
					</div>
					<!-- END HERO SECTION -->
				</div></div>';
}