<?php
/**
 * Genesis Framework.
 *
 * WARNING: This file is part of the core Genesis Framework. DO NOT edit this file under any circumstances.
 * Please do all modifications in the form of a child theme.
 *
 * @package Genesis\Templates
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://my.studiopress.com/themes/genesis/
 */
?>
<footer class="smoke-bg">

				<div class="goTop"><a href="#hero"><img src="'.get_stylesheet_directory_uri().'/images/gototop.png" width="56px;"></a></div>

					<div class="container">

						<div class="row text-center">

							<!-- Footer menu -->



							<div class="clearfix"></div>

							<div class="divide-lg"></div>								

							<div class="col-sm-6 col-xs-12">
								<?php dynamic_sidebar('left-footer'); ?>
							</div>

							<div class="col-sm-6 col-xs-12 contact">
								<?php dynamic_sidebar('right-footer'); ?>
							</div>

				<div class="col-sm-12">

					<div class="divide-md"></div>

					<p><small>&copy;2016-2017 TreeHouseStudio. All Rights Reserved.</small></p>

				</div>

				<div class="clearfix"></div>

				<div class="divide-xs"></div>

			</div>

		</div>

	</footer>
<?php
genesis_structural_wrap( 'site-inner', 'close' );
genesis_markup( array(
	'close'   => '</div>',
	'context' => 'site-inner',
) );

do_action( 'genesis_before_footer' );
do_action( 'genesis_footer' );
do_action( 'genesis_after_footer' );

genesis_markup( array(
	'close'   => '</div>',
	'context' => 'site-container',
) );

 do_action( 'genesis_after' );

wp_footer(); // We need this for plugins.
//do_action( 'genesis_my_footer' );
?>
		<!-- Google analytics -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'YOUR GOOGLE ANALYTICS CODE']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})(); 
		</script>
		<!-- End scripts -->
<?php genesis_markup( array(
	'close'   => '</body>',
	'context' => 'body',
) );

?>
</html>
