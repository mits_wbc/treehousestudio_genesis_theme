<?php

/**

 * Genesis Sample.

 *

 * This file adds functions to the Genesis Sample Theme.

 *

 * @package Genesis Sample

 * @author  StudioPress

 * @license GPL-2.0+

 * @link    http://www.studiopress.com/

 */



//* Start the engine

include_once( get_template_directory() . '/lib/init.php' );



//* Setup Theme

include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );



//* Set Localization (do not remove)

load_child_theme_textdomain( 'genesis-sample', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'genesis-sample' ) );



//* Add Image upload and Color select to WordPress Theme Customizer

require_once( get_stylesheet_directory() . '/lib/customize.php' );



//* Include Customizer CSS

include_once( get_stylesheet_directory() . '/lib/output.php' );



//* Child theme (do not remove)

define( 'CHILD_THEME_NAME', 'Genesis Sample' );

define( 'CHILD_THEME_URL', 'http://www.studiopress.com/' );

define( 'CHILD_THEME_VERSION', '2.2.4' );



//* Enqueue Scripts and Styles

add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );

function genesis_sample_enqueue_scripts_styles() {



	wp_enqueue_style( 'genesis-sample-fonts', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700', array(), CHILD_THEME_VERSION );

	wp_enqueue_style( 'dashicons' );



	wp_enqueue_script( 'genesis-sample-responsive-menu', get_stylesheet_directory_uri() . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0', true );

	$output = array(

		'mainMenu' => __( 'Menu', 'genesis-sample' ),

		'subMenu'  => __( 'Menu', 'genesis-sample' ),

	);

	wp_localize_script( 'genesis-sample-responsive-menu', 'genesisSampleL10n', $output );



}



//* Add HTML5 markup structure

add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );



//* Add Accessibility support

add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );



//* Add viewport meta tag for mobile browsers

add_theme_support( 'genesis-responsive-viewport' );



//* Add support for custom header

add_theme_support( 'custom-header', array(

	'width'           => 600,

	'height'          => 160,

	'header-selector' => '.site-title a',

	'header-text'     => false,

	'flex-height'     => true,

) );



//* Add support for custom background

add_theme_support( 'custom-background' );



//* Add support for after entry widget

add_theme_support( 'genesis-after-entry-widget-area' );



//* Add support for 3-column footer widgets

add_theme_support( 'genesis-footer-widgets', 3 );



//* Add Image Sizes

add_image_size( 'featured-image', 720, 400, TRUE );



//* Rename primary and secondary navigation menus

add_theme_support( 'genesis-menus' , array( 'primary' => __( 'After Header Menu', 'genesis-sample' ), 'secondary' => __( 'Footer Menu', 'genesis-sample' ) ) );



//* Reposition the secondary navigation menu

remove_action( 'genesis_after_header', 'genesis_do_subnav' );

add_action( 'genesis_footer', 'genesis_do_subnav', 5 );



//* Reduce the secondary navigation menu to one level depth

add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );

function genesis_sample_secondary_menu_args( $args ) {



	if ( 'secondary' != $args['theme_location'] ) {

		return $args;

	}



	$args['depth'] = 1;



	return $args;



}



//* Modify size of the Gravatar in the author box

add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );

function genesis_sample_author_box_gravatar( $size ) {



	return 90;



}



//* Modify size of the Gravatar in the entry comments

add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );

function genesis_sample_comments_gravatar( $args ) {



	$args['avatar_size'] = 60;



	return $args;



}

//Footer Menu
function treehousestudio_header_menu () {
 	$args = array( 
		'theme_location'  => 'header_menu', 
 		'container'       => 'div',
 		'container_id'	  => 'navbar-collapse', 	
		'container_class' => 'collapse navbar-collapse',
		'menu_class'      => 'nav navbar-nav navbar-right', 
		'depth'           => 1, //change to 0 for submenu levels
		'data-start'	  => 'color: rgb(255, 255, 255);',  	
		); 
		
	wp_nav_menu( $args );
}
add_theme_support ( 'genesis-menus' , array ( 
                                      'primary' => 'Primary Navigation Menu' , 
                                      'secondary' => 'Secondary Navigation Menu' ,
                                      'header_menu' => 'Header Navigation Menu' 
                                      ) );
add_action( 'treehouse_header_menu', 'treehousestudio_header_menu' );



// Include CSS

add_action( 'wp_enqueue_scripts', 'wsm_custom_stylesheet' );

function wsm_custom_stylesheet() {

wp_enqueue_style( 'custom-style', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );

wp_enqueue_style( 'custom-style1', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );

wp_enqueue_style( 'custom-style2', 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' );

}



// Include Js

add_action( 'wp_enqueue_scripts', 'treehouse_enqueue_script' );

function treehouse_enqueue_script() {

    wp_enqueue_script( 'follow', get_stylesheet_directory_uri() . '/js/modernizr.js', array( 'jquery' ), '', true );



}



/** Load jQuery and jQuery-ui script just before closing Body tag */

add_action('genesis_after_footer', 'treehouse_script_add_body');

function treehouse_script_add_body() {

      

    wp_register_script( 'jquery-ui1', get_stylesheet_directory_uri() . '/js/jquery.scrollto.min.js', false, null);

    wp_enqueue_script( 'jquery-ui1');



	wp_register_script( 'jquery-ui2', get_stylesheet_directory_uri() . '/js/jquery.easing.js', false, null);

	wp_enqueue_script( 'jquery-ui2');



	wp_register_script( 'jquery-ui3', get_stylesheet_directory_uri() . '/js/jquery.localscroll.min.js', false, null);

	wp_enqueue_script( 'jquery-ui3');



	wp_register_script( 'jquery-ui4', get_stylesheet_directory_uri() . '/js/smoothscroll.js', false, null);

	wp_enqueue_script( 'jquery-ui4');



	wp_register_script( 'jquery-ui5', get_stylesheet_directory_uri() . '/js/ekko-lightbox.min.js', false, null);

	wp_enqueue_script( 'jquery-ui5');



	wp_register_script( 'jquery-ui6', 'http://maps.google.com/maps/api/js?sensor=false	', false, null);

	wp_enqueue_script( 'jquery-ui6');



	wp_register_script( 'jquery-ui7', get_stylesheet_directory_uri() . '/js/jquery.form.js', false, null);

	wp_enqueue_script( 'jquery-ui7');



	wp_register_script( 'jquery-ui8', get_stylesheet_directory_uri() . '/js/jquery.flowuplabels.js', false, null);

	wp_enqueue_script( 'jquery-ui8');



	wp_register_script( 'jquery-ui9', get_stylesheet_directory_uri() . '/js/custom.js', false, null);

	wp_enqueue_script( 'jquery-ui9');

}



// Header code



add_action ( 'genesis_my_header', 'genesis_mycode_header', 5 );

function genesis_mycode_header()

{

echo '<div class="main" id="hero">

			<div class="mask-layer">

				<div class="col-lg-12 col-md-12">

					<div class="navbar navbar-default navbar-transparent" role="navigation" data-start="padding: 10px 0px; background: rgba(255, 255, 255, .1); border-color: rgba(255, 255, 255, .1);" data-20p="padding: 0px 0px; background: rgba(255, 255, 255, .95); border-color: rgba(231, 231, 231, 1);">

						<div class="container">

							<div class="navbar-header">

								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">

									<span class="sr-only">Toggle navigation</span>

									<span class="icon-bar"></span>

									<span class="icon-bar"></span>

									<span class="icon-bar"></span>

								</button>
							</div>';

}


add_action ( 'genesis_my_header_end', 'genesis_mycode_header_end', 5 );

function genesis_mycode_header_end()

{

echo '			</div>

					</div></div>

					<div>

						<div class="middle_box">

							<img src="'.get_stylesheet_directory_uri().'/images/logo.png" width="232px" alt="">

							<h4>Your home for digital growth</h4>

							<p class="btn-large btn-lined-white"><span></span><a href="#">CONTACT US</a></p>

						</div>

					</div>

					<!-- END HERO SECTION -->

				</div></div>';

}



// Footer Content

add_action ( 'genesis_my_footer', 'genesis_mycode_footer', 5 );

function genesis_mycode_footer() {

	echo '<footer class="smoke-bg">

				<div class="goTop"><a href="#hero"><img src="'.get_stylesheet_directory_uri().'/images/gototop.png" width="56px;"></a></div>

					<div class="container">

						<div class="row text-center">

							<!-- Footer menu -->



							<div class="clearfix"></div>

							<div class="divide-lg"></div>								

							<div class="col-sm-6 col-xs-12"><img src="'.get_stylesheet_directory_uri().'/images/logo.png" width="182px"></div>

							<div class="col-sm-6 col-xs-12 contact">

								<h3>Contact</h3>

								<p>info@treehousestudio.com.au</p>

								<p>+61 2 9516 4810</p>

								<div class="social">

									<ul>

										<li><a href="#"><img src="'.get_stylesheet_directory_uri().'/images/google.png" width="32px;"></a></li>

										<li><a href="#"><img src="'.get_stylesheet_directory_uri().'/images/face.png" width="32px;"></a></li>

										<li><a href="#"><img src="'.get_stylesheet_directory_uri().'/images/twitter.png" width="32px;"></a></li>

										<li><a href="#"><img src="'.get_stylesheet_directory_uri().'/images/in.png" width="32px;"></a></li>

									</ul>

								</div>

							</div>

							

							<!-- Social networks -->

				<!-- <div class="col-sm-12 divide-xs">

					<a class="facebook" href="http://www.facebook.com/awerest" target="_blank">Facebook</a>

					<a class="twitter" href="http://www.twitter.com/awerest" target="_blank">Twitter</a>

					<a class="google" href="https://plus.google.com/u/0/104533843132286032110/posts" target="_blank">Google+</a>

					<a class="linkedin" href="https://www.linkedin.com/profile/view?id=196747581" target="_blank">LinkedIn</a>

					<a class="instagram" href="http://www.instagram.com/awerest" target="_blank">Instagram</a>

					<a class="pinterest" href="http://www.pinterest.com/awerest" target="_blank">Pinterest</a>

				</div> -->

				<!-- Copyright -->

				<div class="col-sm-12">

					<div class="divide-md"></div>

					<p><small>&copy;2016-2017 TreeHouseStudio. All Rights Reserved.</small></p>

				</div>

				<div class="clearfix"></div>

				<div class="divide-xs"></div>

			</div>

		</div>

	</footer>';	

}
genesis_register_sidebar( array(
	'id'		=> 'left-footer',
	'name'		=> __( 'Left Footer Widget', 'ths1' ),
	'description'	=> __( 'This is the widget area for a footer.', 'ths1' ),
) );
genesis_register_sidebar( array(
	'id'		=> 'right-footer',
	'name'		=> __( 'Right Footer Widget', 'ths2' ),
	'description'	=> __( 'This is the widget area for a footer.', 'ths2' ),
) );
